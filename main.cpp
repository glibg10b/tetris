#include "random.h"

#include <locale.h>
#include <curses.h>

#include <array>
#include <cstddef>
#include <ranges>
#include <thread>

struct TetriminoType {
	const std::array<std::array<bool, 4>, 4> shape{};
	const chtype color{};
};

namespace ColorPair {
	enum {
		red = 1,
		yellow,
		green,
		blue,
		max
	};
}

constexpr std::array<TetriminoType, 4> pieceTypes{{
	{{
			0,0,0,0,
			0,1,1,0,
			0,1,1,0,
			0,0,0,0
		},
		COLOR_PAIR(ColorPair::yellow)
	},
	{{
			0,0,0,0,
			0,0,0,0,
			0,0,0,0,
			1,1,1,1
		},
		COLOR_PAIR(ColorPair::cyan)
	},
	{{
		 	0,0,0,0,
			0,0,0,0,
			0,1,0,0,
			1,1,1,0
		},
		COLOR_PAIR(ColorPair::magenta)
	},
	{{
		 	0,0,0,0,
			1,1,0,0,
			0,1,1,0,
			0,0,0,0
		},
		COLOR_PAIR(ColorPair::blue)
	}
}};

class Tetrimino {
public:
	Tetrimino(const TetriminoType& type): m_type{ type }
	{}

	void rotate() {
		m_rotation = (m_rotation + 1) % 4;
	}

	void draw() const {
		switch (m_rotation)
		{
		case 0:
			for (const auto& row: m_type.shape)
			{
				for (auto square: row) draw(square);
				newline();
			}
			break;
		case 1:
			for (std::size_t col{ 0 };
				col < m_type.shape[0].size(); ++col)
			{
				for (const auto& row
					: m_type.shape | std::views::reverse)
					draw(row[col]);
				newline();
			}
			break;
		case 2:
			for (const auto& row: m_type.shape
					| std::views::reverse)
			{
				for (auto square: row | std::views::reverse)
					draw(square);
				newline();
			}
			break;
		case 3:
			for (auto col{ std::ssize(m_type.shape[0])-1 };
				col >= 0; --col)
			{
				for (const auto& row: m_type.shape)
					draw(row[
						static_cast<std::size_t>(col)
					]);
				newline();
			}
			break;
		}
	}
private:
	const TetriminoType& m_type{};
	int m_rotation{ 0 }; // The piece's rotation in degrees / 90
	void draw(auto square) const {
		if (square)
		{
			addch(ACS_BLOCK | m_type.color);
			addch(ACS_BLOCK | m_type.color);
		}
		else
		{
			move( getcury(stdscr), getcurx(stdscr)+1);
			move( getcury(stdscr), getcurx(stdscr)+1);
		}
	}
	static void newline() {
		move(getcury(stdscr)+1, getcurx(stdscr)-8);
	}
};

void head() {
	setlocale(LC_ALL, "");
	initscr();
	start_color();
	use_default_colors();
	init_pair(ColorPair::red, COLOR_RED, -1);
	init_pair(ColorPair::yellow, COLOR_YELLOW, -1);
	init_pair(ColorPair::green, COLOR_GREEN, -1);
	init_pair(ColorPair::blue, COLOR_BLUE, -1);
	cbreak(); 
	noecho();
	intrflush(stdscr, false);
	keypad(stdscr, true);
}

void tail() {
	endwin();
}

int fail(std::string_view msg, int ret) {
	tail();
	std::cout << msg << '\n';
	return ret;
}

int main() {
	head();
	if (has_colors() == false)
		fail("Your terminal doesn't support colors", 1);

	while (true)
	{
		move(random<0,23>(),random<0,79>());

		Tetrimino piece{ pieceTypes[random<0, pieceTypes.size()-1>()] };
		for (auto i{1}; i <= random<0,3>(); ++i) piece.rotate();
		piece.draw();

		using namespace std::chrono_literals;
		std::this_thread::sleep_for(200ms);

		refresh();
	}
	tail();
}
