
BUILDOPTS=-O2 -DNDEBUG

WARNINGOPTS=-Wall -Weffc++ -Wextra -Wsign-conversion
ERROROPTS=--pedantic-errors

a.out: main.cpp
	c++  ${WARNINGOPTS}  ${ERROROPTS}  ${BUILDOPTS}  --std=c++20 -lcurses -ltinfo main.cpp static.cpp
