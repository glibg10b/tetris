#pragma once

#include <random>
#include <type_traits> // common_type_t

extern std::mt19937 g_mt;

template <
	auto Min,
	auto Max,
	typename T = std::common_type_t<decltype(Min), decltype(Max)>
>
auto& getDist() {
	static auto dist{
		std::uniform_int_distribution<T>{ Min, Max } };
	return dist;
}

template <auto Min, auto Max>
auto random() {
	return getDist<Min, Max>()(g_mt);
}
